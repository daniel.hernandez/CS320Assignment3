
#include<stdio.h>

int main(int argc, char *argv[])
{
    FILE *file1, *file2;
    char ch;
    int offset = (int)(argv[1][0] - '0');
    char filename[256];
    char lower = 65, upper = 97;
    sprintf(filename, "%s.dec", argv[2]);

    file1 = fopen(argv[2], "r");
    file2 = fopen(filename, "w");

    while(1)
    {
        ch = fgetc(file1);
        if(ch == EOF)
        {
            break;
        }
        else
        {
	        if(ch == ' ')
	        {
	            fputc(ch, file2);
	        }
	        else
	        {
                ch = ch - offset;
                if(ch < lower || (ch < upper && ch > lower+27))
                {
                    ch = ch + 26;
                }
                fputc(ch, file2);
	        }
        }
    }
    fclose(file1);
    fclose(file2);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    char plaintxt;
    char ciphertxt = argv[1][0];
    int  distance;

    scanf("%c", &plaintxt);
    distance = abs(plaintxt - ciphertxt);
    if(distance > 13)
    {
        distance = 26 - distance;
    }

    printf( "%d\n", distance);

    return 0;
}

#!/bin/bash

echo "Assignment #3-1, Daniel Hernandez, dhernandez92105@yahoo.com"

file="$1"
SERVER="$2"
USER="$3"

USER=`echo $USER | tr '[:lower:]' '[:upper:]'`

loginInfo=`grep "$USER" $file`
password=`echo $loginInfo | cut -d "," -f3`

USER=`echo $USER | cut -d " " -f1 | tr '[:upper:]' '[:lower:]'`

expect -c "
    log_user 0
    spawn scp -r $USER@$SERVER: .
    expect yes/no { send yes\r ; exp_continue }
    expect password: { send $password\r }
    sleep 1
    exit
"
find encryptor
find *.enc

CS320 Assignment #3
===================

Objective
---------

	This assignment is intended to show the intricacies of working with two programming languages in cooperation.
	In this case, we are working with C and Bash in order to login to a server, get files, and decipher those
	files, to show how certain languages work together.

Programs
--------
**_Names_**

	prog3_1.sh

	prog3_2.c

	prog3_2.sh

	prog3_3.c

	prog3_3.sh

**_Description_**

*prog3_1.sh*

	This bash script logins to a server to transfer files from a user's home 
	directory to your home directory. Outputs name of files transferred. Takes 3 command line
	arguments (filename, server/server ip, "user's name").
	
	COMPILE:
	./prog3_1 filename server "Firstname Lastname"

*prog3_2.c*

	This C program is intended to find the numerical difference between two letters 
	and prints that difference to standard out. Takes a single command line argument and a 
	single character to standard in.

    COMPILE:
    gcc prog3_2.c -o prog3_2
    echo "char" | ./prog3_2 char
    
*prog3_2.sh*

	This bash script takes an arbitrary character to pass thru the encryptor executable.
	It sends the output character given by the executable, and the first character, to prog3_2 
	to find the difference of those two characters. Outputs the numerical difference of those
	two letters.

    COMPILE:
    ./prog3_2.sh
    
*prog3_3.c*

	This C program deciphers a file, encrypted by the Caesar cipher. It takes a command line
	argument, diplayed by prog3_2.sh and a file name, in order to find the offset of each character 
	in the file. Creates a new deciphered file, with the extension .dec.

    COMPILE:
    gcc prog3_3.c -o prog3_3
    ./prog3_3 #
    
*prog3_3.sh*

	This bash script runs through all .enc files in the current directory to be deciphered by
	prog3_3.Takes a command line argument of the offset found in prog3_2.sh. Deciphers all the .enc
	files and creates new .dec files. Displays new deciphered files.

    COMPILE:
    ./prog3_3.sh #
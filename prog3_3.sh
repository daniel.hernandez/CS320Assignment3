#!/bin/bash

echo -e "Assignment 3-3, Daniel Hernandez, dhernandez92105@yahoo.com"

offset=$1

for i in *.enc; do
    ./prog3_3 $1 $i
    filename=`echo $i".dec"`
    newfile=`echo $filename | sed 's/.enc//'`
    mv $filename $newfile
    echo $newfile
done
